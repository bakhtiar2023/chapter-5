const fs = require("fs");
const md5 = require("md5");

loadData = () => {
  const fileBuffer = fs.readFileSync("daftarUser.json", "utf-8");
  const daftarUsers = JSON.parse(fileBuffer);
  return daftarUsers;
};

const daftarUsers = loadData();

addData = (username, email, password) => {
  for (i = daftarUsers.length; i <= daftarUsers.length; i++) {
    i++;
    daftarUsers.push({
      id: i.toString(),
      username,
      email,
      password: md5(password),
    });
    fs.writeFileSync("daftarUser.json", JSON.stringify(daftarUsers));
  }
};

findUsername = (username) => {
  const findeUsername = daftarUsers.find(user => {
    return user.username == username;
  });
  return findeUsername
};

findEmail = (email) => {
  const findEmail = daftarUsers.find(user => {return user.email == email});
  return findEmail
};

findUserDataForLogin = (username,password) =>{
    const findForLogin = daftarUsers.find(user =>{return user.username == username && user.password == md5(password)});
    return findForLogin
}

module.exports = {addData,findUsername,findEmail,findUserDataForLogin}