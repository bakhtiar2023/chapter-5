const express = require('express');
const app = express();
const {signUpValidator,loginValidator} = require('./validation')
const port = 8000;


app.use(express.json());

app.get('/users', (req,res)=>{
    res.send(daftarUser);
})

app.post('/signup',(req,res)=>{
    const {username,email,password} = req.body;
    const signUpResult = signUpValidator(username,email,password);
    res.send(signUpResult)
})

app.post('/login', (req,res)=>{
    const {username,password} =req.body;
    const loginResult = loginValidator(username,password);
    res.send(loginResult)
})

app.listen(port)